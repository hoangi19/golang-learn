package configs

import (
	"os"
	"log"
	"github.com/joho/godotenv"
	"fmt"
)

var DB_URL = build_db_url()

func build_db_url() string {
	err := godotenv.Load("../.env")
	if err != nil {
		log.Fatal("Error when get enviroment. Err: %s", err)
	}

	book_crud_db_user := os.Getenv("BOOK_CRUD_DB_USER")
	book_crud_db_password := os.Getenv("BOOK_CRUD_DB_PASSWORD")
	book_crud_db_dbname := os.Getenv("BOOK_CRUD_DB_DBNAME")
	book_crud_db_host := os.Getenv("BOOK_CRUD_DB_HOST")
	book_crud_db_port := os.Getenv("BOOK_CRUD_DB_PORT")
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", 
						book_crud_db_user, book_crud_db_password,
						book_crud_db_host, book_crud_db_port,
						book_crud_db_dbname)
}

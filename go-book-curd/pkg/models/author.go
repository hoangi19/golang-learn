package models

import (
	"gorm.io/gorm"
)

type Author struct {
	gorm.Model
	// Id uint `gorm:"primaryKey"`
	Name string `json:"name"`
}

func init() {
	db = GetDB()
	db.AutoMigrate(&Author{})
}
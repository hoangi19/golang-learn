package models

import (
	// "gitlab.com/hoangi19/go-book-crud/pkg/models"
	"gorm.io/gorm"
	"fmt"
)

type Book struct {
	gorm.Model
	// Id uint `gorm:"primaryKey"`
	Name string `json:"name"`
	AuthorId uint `json:"author_id"`
	Author Author `gorm:"references:id"`
}

func init() {
	db = GetDB()
	db.AutoMigrate(&Book{})
}

func GetAllBooks() ([]Book, error) {
	db = GetDB()
	var books []Book
	err := db.Model(&Book{}).Preload("Author").Find(&books).Error
	return books, err
}

func GetBookByID(id uint) (Book, error) {
	db = GetDB()
	var book Book
	err := db.Model(&Book{}).Preload("Author").First(&book, id).Error
	return book, err
}

func DeleteBook(id uint) (error) {
	db = GetDB()
	err := db.Delete(&Book{}, id).Error
	return err
}

func UpdateBookByID(id uint, book Book) (Book, error) {
	dbBook, _ := GetBookByID(id)
	fmt.Println(book.Name)
	if book.Name != "" {
		dbBook.Name = book.Name
	}
	// if book.AuthorId != nil {
	// 	dbBook.AuthorId = book.AuthorId
	// }
	db = GetDB()
	db.Save(&dbBook)
	return dbBook, nil
}
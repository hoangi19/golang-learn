package models

import (
	"gorm.io/gorm"
	"gitlab.com/hoangi19/go-book-crud/pkg/configs"
	"gorm.io/driver/mysql"
)

var db *gorm.DB

func Connect() {
	d, err := gorm.Open(mysql.Open(configs.DB_URL), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db = d
}

func GetDB() *gorm.DB {
	if db == nil {
		Connect()
	}
	// if err = db.Ping(); err != nil {
	// 	Connect()
	// }
	return db
}
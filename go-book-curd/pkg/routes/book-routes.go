package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/hoangi19/go-book-crud/pkg/controllers"
)

var RegisterBookRoutes = func (r *mux.Router) {
	r.HandleFunc("/book/", controllers.CreateBooks).Methods("POST") 
	r.HandleFunc("/book/", controllers.GetBooks).Methods("GET") 
	r.HandleFunc("/book/{id}", controllers.GetBooksByID).Methods("GET")
	r.HandleFunc("/book/{id}", controllers.UpdateBookByID).Methods("PUT")
	r.HandleFunc("/book/{id}", controllers.DeleteBook).Methods("DELETE")
}
package controllers

import (
	"net/http"
	"gitlab.com/hoangi19/go-book-crud/pkg/models"
	"encoding/json"
	"github.com/gorilla/mux"
	"strconv"
	"gitlab.com/hoangi19/go-book-crud/pkg/utils"
)

func CreateBooks(w http.ResponseWriter, r *http.Request) {

}

func GetBooks(w http.ResponseWriter, r *http.Request) {
	allBook, _ := models.GetAllBooks()
	res,_ := json.Marshal(allBook)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func GetBooksByID(w http.ResponseWriter, r *http.Request) {
	var params = mux.Vars(r)
	id, _ := strconv.ParseUint(params["id"], 10, 32)
	book,_  := models.GetBookByID(uint(id))
	res, _ := json.Marshal(book)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
	var params = mux.Vars(r)
	id, _ := strconv.ParseUint(params["id"], 10, 32)
	models.DeleteBook(uint(id))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}

func UpdateBookByID(w http.ResponseWriter, r *http.Request) {
	var params = mux.Vars(r)
	id, _ := strconv.ParseUint(params["id"], 10, 32)
	var book = &models.Book{}
	utils.ParseBody(r, book)
	bookDetail, _ := models.UpdateBookByID(uint(id), *book)
	res, _ := json.Marshal(bookDetail)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)

}


package main

import(
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"gitlab.com/hoangi19/go-book-crud/pkg/routes"
	_ "gitlab.com/hoangi19/go-book-crud/pkg/models"
)

func main(){
	r := mux.NewRouter()
	routes.RegisterBookRoutes(r)
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe("localhost:9010", r))
}
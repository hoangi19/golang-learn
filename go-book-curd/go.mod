module gilab.com/hoangi19/go-book-crud

go 1.21.0

replace gitlab.com/hoangi19/go-book-crud => ./

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.5.1
	gitlab.com/hoangi19/go-book-crud v0.0.0-00010101000000-000000000000
	gorm.io/driver/mysql v1.5.1
	gorm.io/gorm v1.25.4
)

require (
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)
